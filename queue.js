let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}


function enqueue(item){
    return collection.push(item);
}


function dequeue(){
    return collection.shift();
}


function front(){
    return collection[0];
}


function size(){
    return collection.length;
}


function isEmpty(){
    return (collection.length === 0);
}




// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};